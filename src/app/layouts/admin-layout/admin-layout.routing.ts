import { Routes } from '@angular/router';
import { MapsComponent } from '../../components/maps/maps.component';
import { ChartComponent } from 'app/components/chart-management/chart/chart.component';
import { AddUserComponent } from 'app/components/user-management/add-user/add-user.component';
import { AddDeviceComponent } from 'app/components/device-management/add-device/add-device.component';
import { ViewDeviceComponent } from 'app/components/device-management/view-device/view-device/view-device.component';
import { EditDeviceComponent } from 'app/components/device-management/edit-device/edit-device.component';
import { AlertComponent } from 'app/components/alert-mangement/alert/alert.component';
import { EditUserComponent } from 'app/components/user-management/edit-user/edit-user.component';
import { EditAlertComponent } from 'app/components/alert-mangement/edit-alert/edit-alert.component';
import { ChartEditComponent } from 'app/components/chart-management/chart-edit/chart-edit.component';
import { DeviceAllocationComponent } from 'app/components/device-management/device-allocation/device-allocation.component';
import { EditDeviceAllocationComponent } from 'app/components/device-management/edit-device-allocation/edit-device-allocation.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'chart', component: ChartComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'add-user', component: AddUserComponent },
    { path: 'add-device', component: AddDeviceComponent },
    { path: 'edit-user/:user_id', component: EditUserComponent },
    { path: 'view-device/:device_id', component: ViewDeviceComponent },
    { path: 'edit-device/:device_id', component: EditDeviceComponent },
    { path: 'alerts', component: AlertComponent },
    { path: 'edit-alert/:alert_id', component: EditAlertComponent },
    { path: 'edit-chart/:chart_id', component: ChartEditComponent },
    { path: 'device-allocation', component: DeviceAllocationComponent, },
    { path: 'edit-device-allocation/:device_id', component: EditDeviceAllocationComponent },
];
