import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { MapsComponent } from '../../components/maps/maps.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { ListboxModule } from 'primeng/listbox';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ChartComponent } from 'app/components/chart-management/chart/chart.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { PaginatorModule } from 'primeng/paginator';
import { AddUserComponent } from 'app/components/user-management/add-user/add-user.component';
import { AddDeviceComponent } from 'app/components/device-management/add-device/add-device.component';
import { TableModule } from 'primeng/table';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { HighchartsChartModule } from 'highcharts-angular';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ViewDeviceComponent } from 'app/components/device-management/view-device/view-device/view-device.component';
import { EditDeviceComponent } from 'app/components/device-management/edit-device/edit-device.component';
import { AlertComponent } from 'app/components/alert-mangement/alert/alert.component';
import { EditUserComponent } from 'app/components/user-management/edit-user/edit-user.component';
import { EditAlertComponent } from 'app/components/alert-mangement/edit-alert/edit-alert.component';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { MultiSelectModule } from 'primeng/multiselect';
import { ChartEditComponent } from 'app/components/chart-management/chart-edit/chart-edit.component';
import { ToolbarModule } from 'primeng/toolbar';
import { DeviceAllocationComponent } from 'app/components/device-management/device-allocation/device-allocation.component';
import { EditDeviceAllocationComponent } from 'app/components/device-management/edit-device-allocation/edit-device-allocation.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    ButtonModule,
    ProgressSpinnerModule,
    ListboxModule,
    AccordionModule,
    ToastModule,
    PaginatorModule,
    TableModule,
    OverlayPanelModule,
    ConfirmDialogModule,
    HighchartsChartModule,
    ToggleButtonModule,
    MultiSelectModule,
  ],
  declarations: [
    MapsComponent,
    ChartComponent,
    AddUserComponent,
    AddDeviceComponent,
    ViewDeviceComponent,
    EditDeviceComponent,
    AlertComponent,
    EditUserComponent,
    EditAlertComponent,
    ChartEditComponent,
    DeviceAllocationComponent,
    EditDeviceAllocationComponent
  ],
  providers: [MessageService, ConfirmationService],
})

export class AdminLayoutModule { }
