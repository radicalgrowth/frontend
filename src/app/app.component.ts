import { Component } from '@angular/core';
import { SharedService } from './Services/shared.service';
import { LocalStorageService } from './Services/Storage/local-storage.service';
import { StorageConstants } from './Services/Storage/storage-contants';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    public sharedService: SharedService,
    private localStorageService: LocalStorageService,
  ) { }
  ngOnInit() {
    this.checkUserRole();
  }
  checkUserRole() {
    let role = this.sharedService.LoggedUserConfiguration.userRole;
    if (role.length <= 0) {
      let data = this.localStorageService.getObject(StorageConstants.CURRENT_USER)
      if (data) {
        this.sharedService.setLoggedInUser(data);
        role = this.sharedService.LoggedUserConfiguration.userRole;
      }
    }
  }
}
