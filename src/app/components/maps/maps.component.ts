import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import { DeviceService } from "app/Services/device.service";
import * as moment from "moment";
import * as Highcharts from "highcharts";
import { SharedService } from "app/Services/shared.service";
declare const google: any;

@Component({
  selector: "app-maps",
  templateUrl: "./maps.component.html",
  styleUrls: ["./maps.component.css"],
})
export class MapsComponent implements OnInit, OnDestroy {
  total_devices;
  DeviceList: any[] = [];
  loading = false;
  map;
  chart;
  selected_device: any;
  selected_parameterstring = "";
  colorPalette = [
    "#d87c7c",
    "#919e8b",
    "#d7ab82",
    "#6e7074",
    "#61a0a8",
    "#efa18d",
    "#787464",
    "#cc7e63",
    "#724e58",
    "#4b565b",
  ];
  chartWindowVisible = false;
  selectedTime = "2021-09-27T00:00:00.000Z";
  refreshTime = "5000";
  interval;

  constructor(
    private deviceService: DeviceService,
    private router: Router,
    private sharedService: SharedService
  ) {}
  getFeilds(device) {
    return new Promise<void>((resolve) => {
      this.deviceService.getParametersByDeviceId(device.device_id).subscribe(
        (res) => {
          this.addMarker(device, res.data);
          resolve();
        },
        (err) => {
          resolve();
        }
      );
    });
  }

  ngOnInit() {
    this.loading = true;
    if (this.sharedService.LoggedUserConfiguration.userRole == "admin") {
      this.deviceService.getAllDevices().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.DeviceList = res.data;
          this.total_devices = res.data.length;
          // Loop through devices
          for (const i in this.DeviceList) {
            //await this.getFeilds(this.DeviceList[i]);
            this.addMarker(this.DeviceList[i], this.DeviceList[i].fields);
            if (i == "0") {
              var tempLatlng = new google.maps.LatLng(
                this.DeviceList[i].location_latitude,
                this.DeviceList[i].location_longitude
              );
              this.map.setCenter(tempLatlng);
            }
          }
          if (res.data.length <= 0) {
            var tempLatlng = new google.maps.LatLng("33.6844", "73.0479");
            this.map.setCenter(tempLatlng);
          }

          this.loading = false;
        }
      });
    } else {
      this.deviceService
        .getDeviceByUserId(this.sharedService.LoggedUserConfiguration.userId)
        .subscribe((res) => {
          if (res.status == 200 && res.data) {
            this.DeviceList = res.data;
            this.total_devices = res.data.length;
            // Loop through devices
            for (const i in this.DeviceList) {
              //await this.getFeilds(this.DeviceList[i]);
              this.addMarker(this.DeviceList[i], this.DeviceList[i].fields);
              if (i == "0") {
                var tempLatlng = new google.maps.LatLng(
                  this.DeviceList[i].location_latitude,
                  this.DeviceList[i].location_longitude
                );
                this.map.setCenter(tempLatlng);
              }
            }
            if (res.data.length <= 0) {
              var tempLatlng = new google.maps.LatLng("33.6844", "73.0479");
              this.map.setCenter(tempLatlng);
            }

            this.loading = false;
          }
        });
    }
    var mapOptions = {
      zoom: 15,
      //center: myLatlng,
      mapTypeId: "terrain",
      scrollwheel: true, //we disable de scroll over the map, it is a really annoing when you scroll through page
      styles: [
        {
          featureType: "water",
          stylers: [
            {
              saturation: 43,
            },
            {
              lightness: -11,
            },
            {
              hue: "#0088ff",
            },
          ],
        },
        {
          featureType: "road",
          elementType: "geometry.fill",
          stylers: [
            {
              hue: "#ff0000",
            },
            {
              saturation: -100,
            },
            {
              lightness: 99,
            },
          ],
        },
        {
          featureType: "road",
          elementType: "geometry.stroke",
          stylers: [
            {
              color: "#808080",
            },
            {
              lightness: 54,
            },
          ],
        },
        {
          featureType: "landscape.man_made",
          elementType: "geometry.fill",
          stylers: [
            {
              color: "#ece2d9",
            },
          ],
        },
        {
          featureType: "poi.park",
          elementType: "geometry.fill",
          stylers: [
            {
              color: "#ccdca1",
            },
          ],
        },
        {
          featureType: "road",
          elementType: "labels.text.fill",
          stylers: [
            {
              color: "#767676",
            },
          ],
        },
        {
          featureType: "road",
          elementType: "labels.text.stroke",
          stylers: [
            {
              color: "#ffffff",
            },
          ],
        },
        {
          featureType: "poi",
          stylers: [
            {
              visibility: "off",
            },
          ],
        },
        {
          featureType: "landscape.natural",
          elementType: "geometry.fill",
          stylers: [
            {
              visibility: "on",
            },
            {
              color: "#b8cb93",
            },
          ],
        },
        {
          featureType: "poi.park",
          stylers: [
            {
              visibility: "on",
            },
          ],
        },
        {
          featureType: "poi.sports_complex",
          stylers: [
            {
              visibility: "on",
            },
          ],
        },
        {
          featureType: "poi.medical",
          stylers: [
            {
              visibility: "on",
            },
          ],
        },
        {
          featureType: "poi.business",
          stylers: [
            {
              visibility: "simplified",
            },
          ],
        },
      ],
    };

    this.map = new google.maps.Map(document.getElementById("map"), mapOptions);

    this.map.addListener("click", (e) => {
      this.graphWindowClose();
    });
  }
  addMarker(props, list_of_fields) {
    let contentString = "";
    let parameter_string = "";
    contentString =
      '<div class="card-header text-white bg-success">' +
      "<h4 class='text-center'><b>" +
      this.removeExtraString(props.device_id) +
      "</b></h4><br /><h5>For More Info Click Icon</h5>";
    /* if (list_of_fields != null) {
      for (let i = 0; i < list_of_fields.length; i++) {
        if (i != 0) {
          contentString += "<br />";
        }
        contentString +=
          this.removeExtraStringParameter(list_of_fields[i]?.name) +
          ": <span class='h5'>" +
          list_of_fields[i]?.last +
          "</span>";
      }
    } */
    contentString += "</div>";
    var marker = new google.maps.Marker({
      position: {
        lat: Number(props.location_latitude),
        lng: Number(props.location_longitude),
      },
      map: this.map,
    });

    // Check for customicon
    if (props.iconImage) {
      // Set icon image
      marker.setIcon(props.iconImage);
    } else {
      var icon = {
        url: "assets/img/logo.png", // url
        scaledSize: new google.maps.Size(30, 30), // scaled size
      };
      marker.setIcon(icon);
    }

    // Check content
    if (props.device_id) {
      var infoWindowDetails = new google.maps.InfoWindow({
        content: contentString,
      });

      marker.addListener("click", () => {
        this.ngOnDestroy();
        parameter_string = "";
        for (let i = 0; i < list_of_fields.length; i++) {
          if (list_of_fields.length == i + 1) {
            parameter_string +=
              "" + list_of_fields[i].name + " AS " + list_of_fields[i].name;
          } else {
            parameter_string +=
              "" +
              list_of_fields[i].name +
              " AS " +
              list_of_fields[i].name +
              ",";
          }
        }
        this.selected_device = props;
        this.selected_parameterstring = parameter_string;
        this.interval = setInterval(() => {
          this.timeChanged(true);
        }, Number(this.refreshTime));

        this.drawGraph(
          parameter_string,
          props.device_id,
          list_of_fields,
          false
        );
      });
      marker.addListener("mouseover", function () {
        infoWindowDetails.open(this.map, marker);
      });
      marker.addListener("mouseout", function () {
        infoWindowDetails.close(this.map, marker);
      });
    }
  }

  drawGraph(parameter_string, device_id, device_parameters, isUpdate) {
    if (parameter_string == null || parameter_string == "") {
      return;
    }
    this.loading = true;
    let series = [];
    this.chartWindowVisible = true;
    this.deviceService
      .getDataByDeviceId(device_id, parameter_string, this.selectedTime)
      .subscribe(async (res2) => {
        var otherdata: any[] = [];
        // ARRAY INTILIZING
        device_parameters.forEach(function () {
          otherdata.push([]);
        });
        for (const element of res2.data) {
          for (let i = 0; i < device_parameters.length; i++) {
            if (element[device_parameters[i].name] != null) {
              otherdata[i].push([
                moment(element.time).unix() * 1000,
                element[device_parameters[i].name],
              ]);
            }
          }
        }
        // console.log({ otherData: otherdata, resData: res2.data });
        for (let i = 0; i < device_parameters.length; i++) {
          let payload = {
            type: undefined,
            name: device_parameters[i].name,
            data: otherdata[i],
          };
          series.push(payload);
        }
        if (series != null) {
          this.graphPopulation(series, isUpdate);
        }

        this.loading = false;
      });
  }
  graphPopulation(series, isUpdate) {
    console.log({ series, isUpdate: isUpdate });
    if (isUpdate == false) {
      this.chart = Highcharts.chart("areasplinecontainer", {
        chart: {
          zoomType: "x",
        },
        title: {
          text: "",
        },
        subtitle: {
          text:
            document.ontouchstart === undefined
              ? "Click and drag in the plot area to zoom in"
              : "Pinch the chart to zoom in",
        },
        xAxis: {
          type: "datetime",
        },
        credits: {
          enabled: false,
        },
        yAxis: {
          title: {
            text: "",
          },
        },
        legend: {
          enabled: false,
        },
        plotOptions: {
          area: {
            fillColor: {
              linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1,
              },
              stops: [
                [0, Highcharts.getOptions().colors[0]],
                [
                  1,
                  Highcharts.color(Highcharts.getOptions().colors[1])
                    .setOpacity(0)
                    .get("rgba")
                    .toString(),
                ],
              ],
            },
            marker: {
              radius: 2,
            },
            lineWidth: 1,
            states: {
              hover: {
                lineWidth: 1,
              },
            },
            threshold: null,
          },
        },
        series: series,
      });
    } else {
      for (let i = 0; i < series.length; i++) {
        if (this.chart.series != null) {
          this.chart.series[i].update(
            {
              data: series[i].data,
            },
            true
          );
        }
      }
    }
  }
  viewDevice() {
    this.router.navigateByUrl(`view-device/${this.selected_device._id}`);
  }
  timeChanged(isUpdate) {
    this.drawGraph(
      this.selected_parameterstring,
      this.selected_device.device_id,
      this.selected_device.fields,
      isUpdate
    );
  }
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  graphWindowClose() {
    this.chartWindowVisible = false;
    this.ngOnDestroy();
  }
  refreshTimeChanged() {
    this.ngOnDestroy();
    this.interval = setInterval(() => {
      this.timeChanged(true);
    }, Number(this.refreshTime));
  }
  devicesInAlarm() {}
  removeExtraString(device) {
    if (device.includes("v3/wahab-test-application-01/devices/")) {
      return device.replace("v3/wahab-test-application-01/devices/", "");
    } else if (device.includes("v3/rgs-test/devices/")) {
      return device.replace("v3/rgs-test/devices/", "");
    } else {
      return device;
    }
  }
  removeExtraStringParameter(parameter) {
    if (parameter.includes("uplink_message_decoded_payload_")) {
      return parameter.replace("uplink_message_decoded_payload_", "");
    } else {
      return parameter;
    }
  }
}
