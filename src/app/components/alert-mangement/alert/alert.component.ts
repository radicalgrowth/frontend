import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AlertService } from "app/Services/alert.service";
import { DeviceService } from "app/Services/device.service";
import { SharedService } from "app/Services/shared.service";
import { MessageService, ConfirmationService } from "primeng/api";
import { Table } from "primeng/table";

@Component({
  selector: "app-alert",
  templateUrl: "./alert.component.html",
  styleUrls: ["./tabledemo.scss"],
})
export class AlertComponent implements OnInit {
  AlertForm: FormGroup;
  submitted = false;
  DeviceList: any[] = [];
  AlertList: any[] = [];
  ParameterList: any[] = [];
  loading = false;
  selectedDevice: string;
  selectedParameter: string;
  @ViewChild("dt") table: Table;
  isActive: boolean[] = [];
  conditions = [
    { label: "Equal to", value: "=" },
    { label: "Greater than", value: ">" },
    { label: "Greater than or equal to", value: ">=" },
    { label: "Less than", value: "<" },
    { label: "Less than or equal to", value: "<=" },
  ];
  actions = [
    { label: "Email notification", value: "email" },
    { label: "SMS notification", value: "phone", disabled: true },
    { label: "Whatsapp notification", value: "whatsapp", disabled: true },
  ];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private deviceService: DeviceService,
    private sharedService: SharedService,
    private alertService: AlertService
  ) {
    this.AlertForm = fb.group({
      alert_name: ["", [Validators.required]],
      device_id: ["", [Validators.required]],
      parameter: ["", [Validators.required]],
      condition: ["", [Validators.required]],
      value: ["", [Validators.required]],
      action: ["", [Validators.required]],
      destination: ["", [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.loading = true;
    if (this.sharedService.LoggedUserConfiguration.userRole === "admin") {
      this.deviceService.getAllDevices().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.DeviceList = res.data;
          this.loading = false;
          this.DeviceList.forEach((element, index) => {
            if (
              element.device_id.includes(
                "v3/wahab-test-application-01/devices/"
              )
            ) {
              this.DeviceList[index].device_id = element.device_id.replace(
                "v3/wahab-test-application-01/devices/",
                ""
              );
            }
          });
        }
      });
    } else {
      this.deviceService
        .getDeviceByUserId(this.sharedService.LoggedUserConfiguration.userId)
        .subscribe((res) => {
          if (res.status == 200 && res.data) {
            this.DeviceList = res.data;
            this.loading = false;
            this.DeviceList.forEach((element, index) => {
              if (
                element.device_id.includes(
                  "v3/wahab-test-application-01/devices/"
                )
              ) {
                this.DeviceList[index].device_id = element.device_id.replace(
                  "v3/wahab-test-application-01/devices/",
                  ""
                );
              }
            });
          }
        });
    }

    if (this.sharedService.LoggedUserConfiguration.userRole === "admin") {
      this.alertService.getAllAlerts().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.AlertList = res.data;
          for (let i = 0; i < res.data.length; i++) {
            this.isActive[i] = res.data[i].active;
          }
          this.loading = false;
        }
      });
    } else {
      const loggedInUser = this.sharedService.LoggedUserConfiguration.userId;
      this.alertService.getAllAlerts().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.AlertList = [];

          for (let i = 0; i < res.data.length; i++) {
            const userId = res.data[i].device_id.user_id;
            if (loggedInUser === userId) {
              this.AlertList.push(res.data[i]);
            }
            this.isActive[i] = res.data[i].active;
          }
          this.loading = false;
        }
      });
    }
  }
  deviceSelected(event) {
    if (event != null) {
      this.AlertForm.patchValue({
        device_id: event._id,
      });
      this.ParameterList = event.fields;
      // this.ParameterList.forEach((element, index) => {
      //   if (element.name.includes("uplink_message_decoded_payload_")) {
      //     this.ParameterList[index].name = element.name.replace(
      //       "uplink_message_decoded_payload_",
      //       ""
      //     );
      //   }
      // });
    } else {
      this.AlertForm.patchValue({
        device_id: "",
      });
      this.ParameterList = [];
      this.parameterSelected(null);
    }
  }
  parameterSelected(event) {
    if (event != null) {
      this.AlertForm.patchValue({
        parameter: event.name,
      });
    } else {
      this.AlertForm.patchValue({
        parameter: "",
      });
    }
  }
  saveAlert() {
    this.submitted = true;

    if (this.AlertForm.invalid) {
      this.notifyError("Error", "Fill in all the required fields :)");
      return;
    }
    let payload = this.AlertForm.value;
    this.alertService.createAlert(payload).subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.AlertList.push(res.data);
        this.notifySuccess("Success", " Alert Added Successfully");
        this.resetForm();
      }
    }),
      (err) => {
        // console.log("ERROR", err);
        this.notifyError("Error", "Error adding Alert");
      };
  }
  resetForm() {
    this.AlertForm.patchValue({
      alert_name: "",
      device_id: "",
      parameter: "",
      condition: "",
      value: "",
      action: "",
      destination: "",
    });
    this.selectedDevice = null;
    this.selectedParameter = null;
    this.submitted = false;
  }
  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail,
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail,
    });
  }
  deleteAlert(index) {
    this.confirmationService.confirm({
      message: "Are you sure that you want delete alert ?",
      accept: () => {
        if (this.table.filteredValue == null) {
          this.alertService
            .deleteAlertById(this.AlertList[index]._id)
            .subscribe(
              (res) => {
                if (res.status == 200 && res.data) {
                  this.AlertList.splice(index, 1);
                  this.notifySuccess(
                    "Success",
                    "Alert deleted successfully :)"
                  );
                }
              },
              (err) => {
                this.notifyError("Error deleting alert");
              }
            );
        } else {
          this.alertService
            .deleteAlertById(this.table.filteredValue[index]._id)
            .subscribe(
              (res) => {
                if (res.status == 200 && res.data) {
                  index = this.AlertList.findIndex(
                    (item) => item._id == this.table.filteredValue[index]._id
                  );
                  this.AlertList.splice(index, 1);
                  this.table.reset();
                  this.notifySuccess(
                    "Success",
                    "Alert deleted successfully :)"
                  );
                }
              },
              (err) => {
                this.notifyError("Error deleting alert");
              }
            );
        }
      },
    });
  }
  editAlert(alertid) {
    this.router.navigateByUrl(`edit-alert/${alertid}`);
  }
  activeChange(e, index) {
    var isChecked = e.checked;
    this.alertService
      .updateAlertById(this.AlertList[index]._id, { active: isChecked })
      .subscribe((res) => {
        this.AlertList[index].active = isChecked;
      });
  }
  removeExtraString(device) {
    if (device) {
      if (device.includes("v3/wahab-test-application-01/devices/")) {
        return device.replace("v3/wahab-test-application-01/devices/", "");
      } else {
        return device;
      }
    }
  }
  removeExtraStringParameter(parameter) {
    if (parameter) {
      if (parameter.includes("uplink_message_decoded_payload_")) {
        return parameter.replace("uplink_message_decoded_payload_", "");
      } else {
        return parameter;
      }
    }
  }
}
