import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'app/Services/alert.service';
import { DeviceService } from 'app/Services/device.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-edit-alert',
  templateUrl: './edit-alert.component.html',
  styleUrls: ['../alert/tabledemo.scss']
})
export class EditAlertComponent implements OnInit {
  AlertForm: FormGroup;
  DeviceList: any[] = [];
  ParameterList: any[] = [];
  loading = false;
  selectedDevice: string;
  selectedParameter: string;
  alert;
  conditions = [
    { label: 'Equal to', value: '=' },
    { label: 'Greater than', value: '>' },
    { label: 'Greater than or equal to', value: '>=' },
    { label: 'Less than', value: '<' },
    { label: 'Less than or equal to', value: '<=' }
  ];
  actions = [
    { label: 'Email notification', value: 'email' },
    { label: 'SMS notification', value: 'phone' },
    { label: 'Whatsapp notification', value: 'whatsapp' },
  ];
  constructor(private fb: FormBuilder, private alertService: AlertService,
    private activatedRoute: ActivatedRoute, private deviceService: DeviceService, private messageService: MessageService,
    private router: Router) {
    this.AlertForm = fb.group({
      alert_name: ["", [Validators.required]],
      device_id: ["", [Validators.required]],
      parameter: ["", [Validators.required]],
      condition: ["", [Validators.required]],
      value: ["", [Validators.required]],
      action: ["", [Validators.required]],
      destination: ["", [Validators.required]]
    });
  }

  async ngOnInit(): Promise<void> {
    const alert_id = this.activatedRoute.snapshot.params["alert_id"];
    await this.getAlert(alert_id);
    this.deviceService.getAllDevices().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.DeviceList = res.data;
        if (this.alert != null) {
          this.selectedDevice = this.DeviceList.find(device => device._id === this.alert.device_id._id);
          this.deviceSelectedOnLoad(this.selectedDevice)
        }
        this.loading = false;
      }
    });

  }
  getAlert(alert_id) {
    return new Promise<void>(resolve => {
      this.alertService.getAlertById(alert_id).subscribe(res => {
        this.alert = res.data;
        this.setFormValues();
        resolve();
      }, err => {
        resolve();
      })
    });
  }
  setFormValues() {
    this.AlertForm.patchValue({
      alert_name: this.alert.alert_name,
      condition: this.alert.condition,
      value: this.alert.value,
      action: this.alert.action,
      destination: this.alert.destination
    });
  }
  deviceSelectedOnLoad(event) {
    if (event != null) {
      this.AlertForm.patchValue({
        device_id: event._id
      })
      this.ParameterList = event.fields
      this.selectedParameter = this.ParameterList.find(parameter => parameter.name === this.alert.parameter);
      this.parameterSelected(this.selectedParameter)
    }
    else {
      this.AlertForm.patchValue({
        device_id: ""
      })
      this.ParameterList = []
      this.parameterSelected(null);
    }
  }
  deviceSelected(event) {
    if (event != null) {
      this.AlertForm.patchValue({
        device_id: event._id
      })
      this.ParameterList = event.fields
    }
    else {
      this.AlertForm.patchValue({
        device_id: ""
      })
      this.ParameterList = []
      this.parameterSelected(null);
    }
  }
  parameterSelected(event) {
    if (event != null) {
      this.AlertForm.patchValue({
        parameter: event.name
      })
    }
    else {
      this.AlertForm.patchValue({
        parameter: ""
      })
    }
  }
  updateAlert() {
    if (this.AlertForm.invalid) {
      this.notifyError("Error", "Fill in all the feilds !");
      return;
    }
    let Payload = this.AlertForm.getRawValue();
    this.alertService.updateAlertById
      (this.alert._id, Payload)
      .subscribe(res => {
        if (res.status == 200 && res.data) {
          this.notifySuccess("Success", "Alert Updated Successfully");
        }
      }),
      err => {
        // console.log("ERROR", err);
        this.notifyError("Error", "Error Updating Alert");
      };
  }

  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail
    });
  }
  cancel() {
    this.router.navigateByUrl(`alerts`)
  }
  removeExtraString(device) {
    if (device.includes("v3/wahab-test-application-01/devices/")) {
      return device.replace(
        "v3/wahab-test-application-01/devices/",
        ""
      );
    }
    else {
      return device;
    }
  }
  removeExtraStringParameter(parameter) {
    if (parameter.includes("uplink_message_decoded_payload_")) {
      return parameter.replace(
        "uplink_message_decoded_payload_",
        ""
      );
    }
    else {
      return parameter;
    }
  }
}
