import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChartService } from 'app/Services/chart.service';
import { DeviceService } from 'app/Services/device.service';
import { LocalStorageService } from 'app/Services/Storage/local-storage.service';
import * as Highcharts from 'highcharts';
import { ConfirmationService, MessageService } from 'primeng/api';
import * as moment from 'moment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
})
export class ChartComponent implements OnInit, OnDestroy {
  ChartForm: FormGroup;
  DeviceList: any[] = [];
  ChartList: any[] = [];
  ParameterList: any[] = [];
  selectedDevice: string;
  submitted = false;
  loading = false;
  charts: any[] = [];
  // selectedTime: Date = new Date();
  selectedTime: string = '2021-09-27T00:00:00.000Z';
  // selectedTime = "24h"; // old value
  // refreshTime: string;
  refreshTime = '10000';
  interval;
  chartsTypes = [
    { label: 'Time Series', value: 'timeseries' },
    // { label: 'line', value: 'line' },
    // { label: 'spline', value: 'spline' },
    // { label: 'area', value: 'area' },
    // { label: 'areaspline', value: 'areaspline' },
    // { label: 'column', value: 'column' },
    // { label: 'bar', value: 'bar' },
    // { label: 'pie', value: 'pie' },
    // { label: 'scatter', value: 'scatter' },
    // { label: 'gauge', value: 'gauge' },
    // { label: 'arearange', value: 'arearange' },
    // { label: 'areasplinerange', value: 'areasplinerange' }
  ];
  constructor(
    private deviceService: DeviceService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private fb: FormBuilder,
    private messageService: MessageService,
    private chartService: ChartService,
    private storageService: LocalStorageService
  ) {
    this.ChartForm = fb.group({
      device_id: ['', [Validators.required]],
      parameters: [''],
      chart_type: [''],
      refreshTime: [''],
      selectedTime: [''],
    });
  }

  ngOnInit(): void {
    this.ngOnDestroy();
    this.loading = true;
    const user = this.storageService.getObject('current_user');
    if (user) {
      this.deviceService.getDeviceByUserId(user.userId).subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.DeviceList = res.data;
          this.loading = false;
        }
      });
    } else {
      this.deviceService.getAllDevices().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.DeviceList = res.data;
          this.loading = false;
        }
      });
    }
    this.getChartData();
    this.ChartForm.valueChanges.subscribe((res) => {});
  }
  getChartData() {
    this.chartService.getAllCharts().subscribe((res) => {
      // console.log(res);
      if (res.status == 200 && res.data) {
        this.ChartList = res.data;
        // console.log({ chartList: this.ChartList, data: res.data });
        this.ChartList.forEach((element, index) => {
          // console.log({ element });
          if (element.device_id != null) {
            let parameter_string = '';
            for (let i = 0; i < element.parameters.length; i++) {
              if (element.device_id != null) {
                if (element.parameters.length == i + 1) {
                  parameter_string +=
                    '' + element.parameters[i] + ' AS ' + element.parameters[i];
                } else {
                  parameter_string +=
                    '' +
                    element.parameters[i] +
                    ' AS ' +
                    element.parameters[i] +
                    ',';
                }
              }

              this.drawGraph(
                parameter_string,
                element.device_id.device_id,
                element.parameters,
                index,
                element
              );
            }
            /* console.log({
              paramString: parameter_string,
              deviceId: element.device_id.device_id,
              index,
              element,
            }); */
          }
        });
        this.loading = false;
      }
    });
  }
  loadChartData() {
    this.ChartList.forEach((element, index) => {
      if (element.device_id != null) {
        let parameter_string = '';
        for (let i = 0; i < element.parameters.length; i++) {
          if (element.parameters.length == i + 1) {
            parameter_string +=
              '' + element.parameters[i] + ' AS ' + element.parameters[i];
          } else {
            parameter_string +=
              '' + element.parameters[i] + ' AS ' + element.parameters[i] + ',';
          }
        }

        this.drawGraph(
          parameter_string,
          element.device_id.device_id,
          element.parameters,
          index,
          element
        );
      }
    });
    this.loading = false;
  }
  deviceSelected(event) {
    // console.log({ selectedDevice: event });
    if (event != null) {
      this.ChartForm.patchValue({
        device_id: event._id,
        parameters: '',
      });
      this.ParameterList = [];
      event.fields.forEach((element) => {
        /* if (element.name === "uplink_message_decoded_payload_gndmoist") {
          element.name = element.name.replace(
            "uplink_message_decoded_payload_gndmoist",
            "uplink_message_decoded_payload_converted_gndmoist"
          );
        } */
        let payload = {
          label: element.name,
          value: element.name,
        };
        this.ParameterList.push(payload);
      });
    } else {
      this.ChartForm.patchValue({
        device_id: '',
        parameters: '',
      });
      this.ParameterList = [];
      this.parameterSelected(null);
    }
  }
  parameterSelected(event) {
    if (event != null) {
      this.ChartForm.patchValue({
        parameters: event.name,
      });
    } else {
      this.ChartForm.patchValue({
        parameters: '',
      });
    }
  }
  generateAreaChart() {
    Highcharts.chart('container', {
      chart: {
        type: 'area',
      },
      accessibility: {
        description:
          'Image description: An area chart compares SM and ST of sensor1.',
      },
      title: {
        text: 'Sensor1 Area Stacked Graph',
      },
      xAxis: {
        allowDecimals: false,
        labels: {
          formatter: function () {
            return this.value + '';
          },
        },
        accessibility: {
          rangeDescription: 'Range: 2020 to 2021.',
        },
      },
      credits: {
        enabled: false,
      },
      yAxis: {
        title: {
          text: 'Sensor1 Data',
        },
        labels: {
          formatter: function () {
            return this.value / 1000 + 'k';
          },
        },
      },
      tooltip: {
        pointFormat:
          '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}',
      },
      plotOptions: {
        area: {
          pointStart: 2020,
          marker: {
            enabled: false,
            symbol: 'circle',
            radius: 2,
            states: {
              hover: {
                enabled: true,
              },
            },
          },
        },
      },
      series: [
        {
          name: 'SM',
          type: undefined,
          data: [24401, 24344],
        },
        {
          name: 'ST',
          type: undefined,
          data: [5000, 60000],
        },
      ],
    });
  }
  genertatetimeSeriesChart() {
    let data = [
      [1611568787, 0.7537],
      [1611568788, 0.7537],
      [1611568789, 0.7559],
      [1167868800000, 0.7631],
      [1167955200000, 0.7644],
      [1168214400000, 0.769],
      [1168300800000, 0.7683],
      [1168387200000, 0.77],
      [1168473600000, 0.7703],
      [1168560000000, 0.7757],
      [1168819200000, 0.7728],
      [1168905600000, 0.7721],
      [1168992000000, 0.7748],
      [1169078400000, 0.774],
      [1169164800000, 0.7718],
      [1169424000000, 0.7731],
      [1169510400000, 0.767],
      [1169596800000, 0.769],
      [1169683200000, 0.7706],
      [1169769600000, 0.7752],
    ];
    Highcharts.chart('container2', {
      chart: {
        zoomType: 'x',
      },
      credits: {
        enabled: false,
      },
      title: {
        text: 'SM over time',
      },
      subtitle: {
        text:
          document.ontouchstart === undefined
            ? 'Click and drag in the plot area to zoom in'
            : 'Pinch the chart to zoom in',
      },
      xAxis: {
        type: 'datetime',
      },
      yAxis: {
        title: {
          text: 'SM',
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [1, Highcharts.getOptions().colors[0]],
            ],
          },
          marker: {
            radius: 2,
          },
          lineWidth: 1,
          states: {
            hover: {
              lineWidth: 1,
            },
          },
          threshold: null,
        },
      },

      series: [
        {
          type: 'area',
          name: 'SM',
          data: data,
        },
      ],
    });
  }
  basicLineChart() {
    Highcharts.chart('container3', {
      title: {
        text: 'Sensor 4',
      },
      credits: {
        enabled: false,
      },
      yAxis: {
        title: {
          text: 'Parameters',
        },
      },

      xAxis: {
        accessibility: {
          rangeDescription: 'Range: 2020 to 2021',
        },
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
          pointStart: 2020,
        },
      },

      series: [
        {
          name: 'SM',
          type: undefined,
          data: [43934, 52503],
        },
        {
          name: 'ST',
          type: undefined,
          data: [24916, 24064],
        },
        {
          name: 'BL',
          type: undefined,
          data: [11744, 17722],
        },
      ],

      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500,
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
              },
            },
          },
        ],
      },
    });
  }
  saveChart() {
    this.submitted = true;

    if (this.ChartForm.invalid) {
      this.notifyError('Error', 'Fill in all the required fields :)');
      return;
    }
    let payload = this.ChartForm.value;
    // console.log({ payloadData: payload });
    this.chartService.createChart(payload).subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.ChartList.push(res.data);
        this.loadChartData();
        this.notifySuccess('Success', ' Chart Added Successfully');
        this.resetForm();
      }
    }),
      (err) => {
        // console.log("ERROR", err);
        this.notifyError('Error', 'Error adding Alert');
      };
  }
  notifyError(summary: string, detail: string = '') {
    this.messageService.add({
      severity: 'error',
      summary: summary,
      detail: detail,
    });
  }
  notifySuccess(summary: string, detail: string = '') {
    this.messageService.add({
      severity: 'success',
      summary: summary,
      detail: detail,
    });
  }
  resetForm() {
    this.ChartForm.patchValue({
      device_id: '',
      parameters: '',
      chart_type: '',
      refreshTime: '',
      selectedTime: '',
    });
    this.ParameterList = [];
    this.selectedDevice = null;
  }
  graphPopulation(
    series,
    index,
    device_id,
    parameter_string,
    chartElement,
    chartService,
    confirmationService,
    router
  ) {
    console.log({
      message: 'in graphPopulation()',
      series,
      index,
      device_id,
      parameter_string,
      chartElement,
    });
    var chartDiv = document.getElementById('chart' + index);
    if (chartDiv == null) {
      //HEADING DIV
      let batteryHeading = parameter_string.includes('battery')
        ? 'Battery'
        : '';
      let moistHeading = parameter_string.includes('gndmoist')
        ? 'Soil-Moisture'
        : '';
      let tempHeading = parameter_string.includes('gndtemp')
        ? 'Soil-Temperature'
        : '';
      let combineText = '';
      if (batteryHeading != '') {
        combineText = 'Battery';
      }
      if (moistHeading != '' && batteryHeading != '') {
        combineText = combineText + ', Soil-Moisture';
      }
      if (batteryHeading != '' && moistHeading != '' && tempHeading != '') {
        combineText = combineText + ', Soil-Temperature';
      }

      var graphHeading = document.createElement('P');
      graphHeading.style.marginTop = '10px';
      // graphHeading.innerText = device_id + " Graph " + parameter_string + ")";
      graphHeading.innerText = `${device_id.split('/')[3].toUpperCase()}`;
      graphHeading.style.fontWeight = 'bold';
      graphHeading.style.display = 'inline-block';
      //BUTTON DELETE
      var btnDelete = document.createElement('BUTTON');
      btnDelete.style.display = 'inline-block';
      btnDelete.style.marginLeft = '50px';
      btnDelete.className = 'btn-danger pi pi-trash';
      btnDelete.onclick = function () {
        confirmationService.confirm({
          message: 'Are you sure that you want delete chart ?',
          accept: () => {
            chartService.deleteChartById(chartElement._id).subscribe(
              (res) => {
                if (res.status == 200 && res.data) {
                  chartDiv.remove();
                  btnDelete.remove();
                  // btnEdit.remove();
                  graphHeading.remove();
                }
              },
              (err) => {
                console.log('Error', err);
              }
            );
          },
        });
        return;
      };
      //BUTTON EDIT
      /* var btnEdit = document.createElement("BUTTON");
      btnEdit.style.display = "inline-block";
      btnEdit.style.marginLeft = "5px";
      btnEdit.className = "btn-warning pi pi-pencil";
      btnEdit.onclick = function () {
        router.navigateByUrl(`edit-chart/${chartElement._id}`);
        return;
      }; */
      //Chart Div
      chartDiv = document.createElement('div');
      chartDiv.id = 'chart' + index;
      chartDiv.style.width = '100%';
      // chartDiv.style.border = "1px solid black";
      document.getElementById('containergraph').appendChild(graphHeading);
      document.getElementById('containergraph').appendChild(btnDelete);
      // document.getElementById("containergraph").appendChild(btnEdit);
      document.getElementById('containergraph').appendChild(chartDiv);
    }
    Highcharts.chart(chartDiv, {
      chart: {
        marginLeft: 40, // Keep all charts left aligned
        spacingTop: 20,
        spacingBottom: 20,
        zoomType: 'x',
      },
      title: {
        text: '',
      },
      subtitle: {
        text: document.ontouchstart === undefined ? '' : '',
      },
      xAxis: {
        type: 'datetime',
      },
      credits: {
        enabled: false,
      },
      yAxis: {
        title: {
          text: '',
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [
                1,
                Highcharts.color(Highcharts.getOptions().colors[1])
                  .setOpacity(0)
                  .get('rgba')
                  .toString(),
              ],
            ],
          },
          marker: {
            radius: 2,
          },
          lineWidth: 1,
          states: {
            hover: {
              lineWidth: 1,
            },
          },
          threshold: null,
        },
      },
      series: series,
    });
  }
  drawGraph(
    parameter_string,
    device_id,
    device_parameters,
    index,
    chartElement
  ) {
    /* console.log({
      message: 'in drawGraph()',
      parameter_string,
      device_id,
      device_parameters,
      index,
      chartElement,
    }); */
    if (parameter_string == null || parameter_string == '') {
      return;
    }
    let series = [];
    this.loading = true;
    // console.log({ message: 'api parameters', device_id, parameter_string });
    this.deviceService
      .getDataByDeviceId(device_id, parameter_string, this.selectedTime)
      .subscribe(async (res2) => {
        if (res2.data.length) {
          // console.log({ message: 'api resultData', data: res2.data });
          let timedata = [];
          var otherdata: any[] = [];
          // ARRAY INTILIZING
          device_parameters.forEach(function () {
            otherdata.push([]);
          });
          // console.log({ message: 'otherdata', otherdata });

          for (const element of res2.data) {
            console.log(element);
            for (let i = 0; i < device_parameters.length; i++) {
              // otherdata[i].push(element[device_parameters[i]])
              otherdata[i].push([
                // moment(element.time).format("dddd, MMMM Do YYYY, h:mm:ss a"),
                moment(element.time).unix() * 1000,
                element[device_parameters[i]],
              ]);
            }
          }
          /* console.log({
            message: 'after loop extracting data',
            otherdata: otherdata,
            device_parameters: device_parameters,
          }); */
          for (let i = 0; i < device_parameters.length; i++) {
            let payload = {
              type: undefined,
              name: device_parameters[i],
              data: otherdata[i],
            };
            series.push(payload);
          }
          // console.log({ message: 'seriesData', series });

          this.graphPopulation(
            series,
            index,
            device_id,
            parameter_string,
            chartElement,
            this.chartService,
            this.confirmationService,
            this.router
          );

          this.loading = false;
        }
      });
    /* console.log({
      paramString: parameter_string,
      deviceID: device_id,
      deviceParam: device_parameters,
      index: index,
      chartEl: chartElement,
      seriesData: series,
    }); */
  }
  timeChanged(event) {
    const currentDateTime = new Date();
    /* console.log({
      eventValue: event.value,
      currentDate: currentDateTime.toISOString(),
      value: currentDateTime.toISOString(
        currentDateTime.setHours(currentDateTime.getHours() - 1)
      ),
    }); */
    /* if (event.value === "5m") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setMinutes(new Date().getMinutes() - 5)
      );
    } else if (event.value === "15m") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setMinutes(new Date().getMinutes() - 15)
      );
    } else if (event.value === "1h") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 1)
      );
    } else if (event.value === "6h") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 6)
      );
    } else if (event.value === "12h") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 12)
      );
    } else if (event.value === "24h") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 24)
      );
    } else if (event.value === "2d") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 48)
      );
    } else if (event.value === "7d") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 168)
      );
    } else if (event.value === "30d") {
      this.selectedTime = currentDateTime.toISOString(
        currentDateTime.setHours(new Date().getHours() - 720)
      );
    } */
    // this.loadChartData();
  }
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  refreshTimeChanged() {
    if (this.interval) {
      clearInterval(this.interval);
    }
    /* this.interval = setInterval(() => {
      this.timeChanged();
    }, Number(this.refreshTime)); */
  }
}
