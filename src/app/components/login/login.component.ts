import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from 'app/Services/shared.service';
import { LocalStorageService } from 'app/Services/Storage/local-storage.service';
import { StorageConstants } from 'app/Services/Storage/storage-contants';
import { UserService } from 'app/Services/user.service';
import { MessageService } from 'primeng/api';
import { ILoggedUserConfiguration } from '../Interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loggedIn = false;
  submitted = false;
  loading = false;
  LoggedUserConfiguration: ILoggedUserConfiguration = {
    userId: '',
    userName: '',
    userRole: '',
    userProfilePicture: '',
    canRead: false,
    canDelete: false,
    canUpdate: false,
    canCreate: false,
  };

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private sharedService: SharedService,
    private storageService: LocalStorageService,
    private messageService: MessageService
  ) {
    this.loginForm = fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
  ngOnInit(): void {
    // const user = this.sharedService.LoggedUserConfiguration;
    const user = this.storageService.getObject('current_user');
    if (user) {
      this.router.navigateByUrl('chart');
    }
    this.clearForm();
  }
  get f() {
    return this.loginForm.controls;
  }
  clearForm() {
    this.loginForm.patchValue({
      email: '',
      password: '',
    });
    this.submitted = false;
  }
  submitLogin() {
    this.submitted = true;
    if (!this.loginForm.invalid) {
      this.loading = true;
      let payload = this.loginForm.value;
      this.userService.loginUser(payload).subscribe(
        (res) => {
          if (res.status == 200 && res.data) {
            let user = res.data.user;
            this.setLoginConfig(
              user._id,
              user.name,
              user.role,
              user.profile_picture,
              user.canCreate,
              user.canDelete,
              user.canUpdate,
              user.canRead
            );
            this.sharedService.setLoggedInUser(this.LoggedUserConfiguration);
            this.storageService.setObject(
              StorageConstants.CURRENT_USER,
              this.LoggedUserConfiguration
            );
            this.clearForm();
            this.router.navigateByUrl('chart');
            this.loading = false;
            this.notifySuccess('Logged In', 'Successfully logged In');
          }
        },
        (err) => {
          this.loading = false;
          //this.authService.session = { user: null, token: null }
          this.notifyError('Invalid', 'Invalid Username or Password !');
        }
      );
    }
  }
  notifyError(summary: string, detail: string = '') {
    this.messageService.add({
      severity: 'error',
      summary: summary,
      detail: detail,
    });
  }
  notifySuccess(summary: string, detail: string = '') {
    this.messageService.add({
      severity: 'success',
      summary: summary,
      detail: detail,
    });
  }
  setLoginConfig(
    userid: string,
    username: string,
    role: string,
    picture: string,
    cancreate: boolean,
    candelete: boolean,
    canupdate: boolean,
    canview: boolean
  ) {
    this.LoggedUserConfiguration.userId = userid;
    this.LoggedUserConfiguration.userName = username;
    this.LoggedUserConfiguration.userRole = role;
    this.LoggedUserConfiguration.userProfilePicture = picture;
    this.LoggedUserConfiguration.canCreate = cancreate;
    this.LoggedUserConfiguration.canDelete = candelete;
    this.LoggedUserConfiguration.canUpdate = canupdate;
    this.LoggedUserConfiguration.canRead = canview;
  }
}
