import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DeviceAllocationService } from 'app/Services/device-allocation.service';
import { DeviceService } from 'app/Services/device.service';
import { SharedService } from 'app/Services/shared.service';
import { UserService } from 'app/Services/user.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
@Component({
  selector: 'app-device-allocation',
  templateUrl: './device-allocation.component.html',
  styleUrls: ['./tabledemo.scss']
})
export class DeviceAllocationComponent implements OnInit {
  DeviceForm: FormGroup;
  submitted = false;
  DeviceList: any[] = [];
  ThingStackDevicesList: any[] = [];
  UserList: any[] = [];
  selectedUser: string;
  filteredDeviceList = [];
  loading = false;
  @ViewChild('dt') table: Table;
  constructor(private fb: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private deviceService: DeviceService,
    private sharedService: SharedService,
    private userService: UserService,
    private deviceAllocationService: DeviceAllocationService) {
    this.DeviceForm = fb.group({
      user_id: ["", [Validators.required]],
      devices: [""],
    });
  }

  ngOnInit(): void {
    this.loading = true;
    this.DeviceForm.valueChanges.subscribe(res => {
      // console.log(res, "Form data")
    })
    this.deviceService.getThingsStackDevices().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.ThingStackDevicesList = res.data;
      }
    })
    this.userService.getAllUsers().subscribe(res => {
      if (res.status == 200 && res.data) {

        this.UserList = res.data;
        this.loading = false;
      }
    });
    this.deviceAllocationService.getAllDeviceAllocations().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.DeviceList = res.data;
        this.filteredDeviceList = res.data;
        this.loading = false;
      }
    });
  }
  get f() {
    return this.DeviceForm.controls;
  }
  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail
    });
  }
  userSelected(event) {
    if (event != null) {
      this.DeviceForm.patchValue({
        user_id: event._id
      })
    }
  }
  allocateDevice() {
    this.submitted = true;

    if (this.DeviceForm.invalid) {
      this.notifyError("Error", "Fill in all the required fields :)");
      return;
    }
    let payload = this.DeviceForm.value;
    this.deviceAllocationService.createDeviceAllocation(payload).subscribe(res => {
      if (res.status == 200 && res.data) {
        this.DeviceList.push(res.data);
        this.notifySuccess("Success", " Devices Allocation Successfully");
        this.resetForm();
      }
    }),
      err => {
        console.log("ERROR", err);
        this.notifyError("Error", "Error allocating Devices");
      };
  }
  resetForm() {
    this.DeviceForm.patchValue({
      user_id: "",
      devices: "",
    });
    this.selectedUser = null;
    this.submitted = false;
  }
  deleteDevice(index) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want delete devices allocation ?',
      accept: () => {
        if (this.table.filteredValue == null) {
          this.deviceAllocationService.deleteDeviceAllocationById(this.DeviceList[index]._id).subscribe(res => {
            if (res.status == 200 && res.data) {
              this.DeviceList.splice(index, 1);
              this.notifySuccess("Success", "Device Allocation deleted successfully :)")
            }
          }, err => {
            this.notifyError("Error deleting device");
          })
        }
        else {
          this.deviceAllocationService.deleteDeviceAllocationById(this.table.filteredValue[index]._id).subscribe(res => {
            if (res.status == 200 && res.data) {
              index = this.DeviceList.findIndex(item => item._id == this.table.filteredValue[index]._id)
              this.DeviceList.splice(index, 1);
              this.table.reset();
              this.notifySuccess("Success", "Device Allocation deleted successfully  :)")
            }
          }, err => {
            this.notifyError("Error deleting device");
          })
        }
      }
    });

  }
  resetFilters() {
    this.table.reset();

  }
  editDevice(deviceid) {
    this.router.navigateByUrl(`edit-device-allocation/${deviceid}`);
  }
  removeExtraString(device) {
    if (device.includes("v3/wahab-test-application-01/devices/")) {
      return device.replace(
        "v3/wahab-test-application-01/devices/",
        ""
      );
    }
    else {
      return device;
    }
  }
}
