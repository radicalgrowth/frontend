import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceAllocationService } from 'app/Services/device-allocation.service';
import { DeviceService } from 'app/Services/device.service';
import { SharedService } from 'app/Services/shared.service';
import { UserService } from 'app/Services/user.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-edit-device-allocation',
  templateUrl: './edit-device-allocation.component.html',
  styleUrls: ['./edit-device-allocation.component.css']
})
export class EditDeviceAllocationComponent implements OnInit {
  DeviceForm: FormGroup;
  submitted = false;
  ThingStackDevicesList: any[] = [];
  UserList: any[] = [];
  selectedUser: string;
  loading = false;
  device;
  constructor(private router: Router,
    private messageService: MessageService,
    private deviceService: DeviceService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private deviceAllocationService: DeviceAllocationService,
    private fb: FormBuilder,) {
    this.DeviceForm = fb.group({
      user_id: ["", [Validators.required]],
      devices: [""],
    });
  }

  ngOnInit(): void {
    const device_id = this.activatedRoute.snapshot.params["device_id"];
    this.getDevice(device_id);
    this.deviceService.getThingsStackDevices().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.ThingStackDevicesList = res.data;
      }
    })
    this.userService.getAllUsers().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.UserList = res.data;
        if (this.device != null) {
          this.selectedUser = this.UserList.find(user => user._id === this.device.user_id?._id);
        }
        this.loading = false;
      }
    });
  }
  get f() {
    return this.DeviceForm.controls;
  }
  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail
    });
  }
  userSelected(event) {
    if (event != null) {
      this.DeviceForm.patchValue({
        user_id: event._id
      })
    }
  }
  getDevice(device_id) {
    return new Promise<void>(resolve => {
      this.deviceAllocationService.getDeviceAllocationById(device_id).subscribe(res => {
        this.device = res.data;
        this.setFormValues();
        resolve();
      }, err => {
        resolve();
      })
    });
  }
  setFormValues() {
    if (this.device.user_id) {
      this.DeviceForm.patchValue({
        user_id: this.device.user_id._id
      });
    }
    this.DeviceForm.patchValue({
      devices: this.device.devices,
    });
  }
  cancel() {
    this.router.navigateByUrl(`device-allocation`)
  }
  editAllocateDevice() {
    this.submitted = true;
    if (this.DeviceForm.invalid) {
      this.notifyError("Error", "Fill in all the feilds !");
      return;
    }
    let Payload = this.DeviceForm.getRawValue();
    this.deviceAllocationService.updateDeviceAllocationById
      (this.device._id, Payload)
      .subscribe(res => {
        if (res.status == 200 && res.data) {
          this.submitted = false;
          this.notifySuccess("Success", " Device Allocation Updated Successfully");
        }
      }),
      err => {
        console.log("ERROR", err);
        this.notifyError("Error", "Error Updating Device Allocation");
      };
  }
}
