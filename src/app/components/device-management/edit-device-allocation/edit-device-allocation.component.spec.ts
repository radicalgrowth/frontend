import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDeviceAllocationComponent } from './edit-device-allocation.component';

describe('EditDeviceAllocationComponent', () => {
  let component: EditDeviceAllocationComponent;
  let fixture: ComponentFixture<EditDeviceAllocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDeviceAllocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDeviceAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
