import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceService } from 'app/Services/device.service';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';
import * as syncCharts from './sync-addition.js';

syncCharts.syncChartsInit(Highcharts);

@Component({
  selector: 'app-view-device',
  templateUrl: './view-device.component.html',
  styleUrls: ['./view-device.component.css'],
})
export class ViewDeviceComponent implements OnInit, OnDestroy {
  device: any = null;
  loading = false;
  parameter_string = '';
  selectedTime = '2021-09-27T00:00:00.000Z';
  refreshTime = '10000';
  interval;
  chart;
  public activity;
  public xData;
  public label;
  colorPalette = [
    '#d87c7c',
    '#919e8b',
    '#d7ab82',
    '#6e7074',
    '#61a0a8',
    '#efa18d',
    '#787464',
    '#cc7e63',
    '#724e58',
    '#4b565b',
  ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private deviceService: DeviceService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    const device_id = this.activatedRoute.snapshot.params['device_id'];

    this.deviceService.getDeviceById(device_id).subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.device = res.data;
        this.initGraph();
      }
    });
  }
  initGraph() {
    this.ngOnDestroy();
    this.parameter_string = '';
    // Modified
    for (let i = 0; i < this.device.fields.length; i++) {
      if (this.device.fields.length == i + 1) {
        this.parameter_string += this.device.fields[i];
      } else {
        this.parameter_string += this.device.fields[i] + ',';
      }
    }
    // Original
    /* for (let i = 0; i < this.device.fields.length; i++) {
      if (this.device.fields.length == i + 1) {
        this.parameter_string +=
          '' + this.device.fields[i] + ' AS ' + this.device.fields[i];
      } else {
        this.parameter_string +=
          '' + this.device.fields[i] + ' AS ' + this.device.fields[i] + ',';
      }
    } */
    /* this.interval = setInterval(() => {
      this.timeChanged();
    }, Number(this.refreshTime)); */

    this.drawGraph(
      this.parameter_string,
      this.device.device_id,
      this.device.fields,
      false
    );
  }
  back() {
    this.router.navigateByUrl(`maps`);
  }
  /*
  drawGraph("uplink_message_decoded_payload_battery AS uplink_message_decoded_payload_battery,uplink_message_decoded_payload_gndmoist AS uplink_message_decoded_payload_gndmoist,uplink_message_decoded_payload_gndmoist2 AS uplink_message_decoded_payload_gndmoist2,uplink_message_decoded_payload_gndtemp AS uplink_message_decoded_payload_gndtemp", "v3/rgs-test/devices/rgs-test-01/up", ["uplink_message_decoded_payload_battery", "uplink_message_decoded_payload_gndmoist", "uplink_message_decoded_payload_gndmoist2", "uplink_message_decoded_payload_gndtemp"], false) 
  parameterString: "uplink_message_decoded_payload_battery AS uplink_message_decoded_payload_battery,uplink_message_decoded_payload_gndmoist AS uplink_message_decoded_payload_gndmoist,uplink_message_decoded_payload_gndmoist2 AS uplink_message_decoded_payload_gndmoist2,uplink_message_decoded_payload_gndtemp AS uplink_message_decoded_payload_gndtemp"
  device_id: "v3/rgs-test/devices/rgs-test-01/up", 
  device_parameters: Array(4)
0: "uplink_message_decoded_payload_battery"
1: "uplink_message_decoded_payload_gndmoist"
2: "uplink_message_decoded_payload_gndmoist2"
3: "uplink_message_decoded_payload_gndtemp"
isUpdate: false

  */
  drawGraph(parameter_string, device_id, device_parameters, isUpdate) {
    /* console.log({
      parameter_string,
      device_id,
      device_parameters,
      isUpdate,
      date: moment(new Date()).add(15, 'minutes').format('YYYY-MM-DD HH:mm:ss'),
    }); */
    if (parameter_string == null || parameter_string == '') {
      return;
    }
    this.loading = true;
    let series = [];
    this.deviceService
      .getDataByDeviceId(device_id, parameter_string, this.selectedTime)
      .subscribe(async (res2) => {
        var otherdata: any[] = [];
        // ARRAY INTILIZING
        device_parameters.forEach(function () {
          otherdata.push([]);
        });
        for (const element of res2.data) {
          for (let i = 0; i < device_parameters.length; i++) {
            if (element[device_parameters[i]] != null) {
              let fetchedDate = moment(element.time)
                .add(5, 'hours')
                .format('YYYY-MM-DD HH:mm:ss');
              /* console.log({
                // data: element,
                // message: "element time",
                // date: fetchedDate,
                // queryDate: moment(fetchedDate).toDate(),
                // timeSimple: Date.parse(fetchedDate),
                // unix: moment(element.time).unix() * 1000,
              }); */
              otherdata[i].push([
                // moment(element.time).format("YYYY-MM-DD HH:mm:ss"),
                // moment(element.time).unix() * 1000,
                Date.parse(fetchedDate),
                element[device_parameters[i]],
              ]);
            }
          }
        }
        for (let i = 0; i < device_parameters.length; i++) {
          // console.log({ field: device_parameters[i] });
          let payload = {};

          payload = {
            type: undefined,
            name: device_parameters[i],
            data: otherdata[i],
          };

          series.push(payload);
        }
        // console.log({ seriesData: series });

        this.graphPopulation(series, isUpdate);

        this.loading = false;
      });
  }
  timeChanged() {
    this.drawGraph(
      this.parameter_string,
      this.device.device_id,
      this.device.fields,
      true
    );
  }
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  refreshTimeChanged() {
    this.ngOnDestroy();
    this.interval = setInterval(() => {
      this.timeChanged();
    }, Number(this.refreshTime));
  }
  graphPopulation(series, isUpdate) {
    // let postFix = '';
    console.log(series);
    /* for (let i = 0; i < series.length; i++) {
      if (series[i].name === 'uplink_message_decoded_payload_battery') {
        postFix = ' %';
      } else if (series[i].name === 'uplink_message_decoded_payload_gndmoist') {
        postFix = ' %';
      } else if (
        series[i].name === 'uplink_message_decoded_payload_gndmoist2'
      ) {
        postFix = ' %';
      } else if (series[i].name === 'uplink_message_decoded_payload_gndtemp') {
        // postFix = ' °C';
        postFix = ' °F';
      }
    } */

    if (isUpdate == false) {
      this.chart = Highcharts.chart('container', {
        chart: {
          zoomType: 'x',
        },
        title: {
          text: '',
        },
        subtitle: {
          text:
            document.ontouchstart === undefined
              ? 'Click and drag in the plot area to zoom in'
              : 'Pinch the chart to zoom in',
        },
        xAxis: {
          type: 'datetime',
        },
        credits: {
          enabled: false,
        },
        yAxis: {
          title: {
            text: '',
          },
          labels: {
            overflow: 'justify',
          },
        },
        legend: {
          enabled: true,
        },
        tooltip: {
          valueSuffix: ' %',
          shared: true,
        },
        plotOptions: {
          area: {
            fillColor: {
              linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1,
              },
              stops: [
                [0, Highcharts.getOptions().colors[0]],
                [
                  1,
                  Highcharts.color(Highcharts.getOptions().colors[1])
                    .setOpacity(0)
                    .get('rgba')
                    .toString(),
                ],
              ],
            },
            marker: {
              radius: 2,
            },
            lineWidth: 1,
            states: {
              hover: {
                lineWidth: 1,
              },
            },
            threshold: null,
          },
        },
        series: series,
      });
    } else {
      for (let i = 0; i < series.length; i++) {
        this.chart.series[i].update(
          {
            data: series[i].data,
          },
          true
        );
      }
    }
  }
}
