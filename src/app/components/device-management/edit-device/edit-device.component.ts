import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceService } from 'app/Services/device.service';
import { UserService } from 'app/Services/user.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-edit-device',
  templateUrl: './edit-device.component.html',
  styleUrls: ['./edit-device.component.css']
})
export class EditDeviceComponent implements OnInit {
  DeviceForm: FormGroup;
  submitted = false;
  UserList: any[] = [];
  loading = false;
  deviceAlreadyExists: boolean;
  selectedUser: string;
  device;
  selected_field_list: any[] = [];
  ThingStackDevicesList: any[] = [];
  ThingStackFeildsList: any[] = [];
  constructor(private fb: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private deviceService: DeviceService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService) {
    this.DeviceForm = fb.group({
      user_id: ["", [Validators.required]],
      device_id: ["", [Validators.required]],
      location_latitude: [""],
      location_longitude: [""],
      fields: [""]
    });
  }

  async ngOnInit(): Promise<void> {
    this.DeviceForm.valueChanges.subscribe(res => {
    })
    this.deviceService.getThingsStackDevices().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.ThingStackDevicesList = res.data;
      }
    })
    this.deviceService.getThingsStackFeilds().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.ThingStackFeildsList = res.data;
      }
    })
    const device_id = this.activatedRoute.snapshot.params["device_id"];
    await this.getDevice(device_id);
    this.userService.getAllUsers().subscribe(res => {
      if (res.status == 200 && res.data) {
        this.UserList = res.data;
        if (this.device != null) {
          this.selectedUser = this.UserList.find(user => user._id === this.device.user_id._id);
        }
        this.loading = false;
      }
    });
    this.DeviceForm.controls.device_id.valueChanges.subscribe(code => {
      this.deviceAlreadyExists = false;
      if (code != '' && code != this.device.device_id) {
        this.deviceService.getDeviceAlreadyRegistered({ deviceid: code }).subscribe(res => {
          if (res.status == 200 && res.data) {
            this.deviceAlreadyExists = false;
          } else {
            this.deviceAlreadyExists = true;
          }
        });
      }
    });
  }
  getDevice(device_id) {
    return new Promise<void>(resolve => {
      this.deviceService.getDeviceById(device_id).subscribe(res => {
        this.device = res.data;
        this.setFormValues();
        resolve();
      }, err => {
        resolve();
      })
    });
  }
  setFormValues() {
    this.DeviceForm.patchValue({
      user_id: this.device.user_id._id,
      device_id: this.device.device_id,
      location_latitude: this.device.location_latitude,
      location_longitude: this.device.location_longitude,
      fields: this.device.fields
    });

  }
  updateDevice() {
    this.submitted = true;

    if (this.deviceAlreadyExists) {
      this.notifyError("Error", "Device Already Registered !");
      return;
    }
    if (this.DeviceForm.invalid) {
      this.notifyError("Error", "Fill in all the feilds !");
      return;
    }
    let Payload = this.DeviceForm.getRawValue();
    this.deviceService.updateDeviceById
      (this.device._id, Payload)
      .subscribe(res => {
        if (res.status == 200 && res.data) {
          this.submitted = false;
          this.notifySuccess("Success", " Device Updated Successfully");
        }
      }),
      err => {
        // console.log("ERROR", err);
        this.notifyError("Error", "Error Updating Device");
      };
  }

  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail
    });
  }
  cancel() {
    this.router.navigateByUrl(`add-device`)
  }
  userSelected(event) {
    if (event != null) {
      this.DeviceForm.patchValue({
        user_id: event._id
      })
    }
  }
}
