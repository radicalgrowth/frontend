import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { DeviceAllocationService } from "app/Services/device-allocation.service";
import { DeviceService } from "app/Services/device.service";
import { SharedService } from "app/Services/shared.service";
import { UserService } from "app/Services/user.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { Table } from "primeng/table";

@Component({
  selector: "app-add-device",
  templateUrl: "./add-device.component.html",
  styleUrls: ["./tabledemo.scss"],
})
export class AddDeviceComponent implements OnInit {
  DeviceForm: FormGroup;
  submitted = false;
  DeviceList: any[] = [];
  ThingStackDevicesList: any[] = [];
  ThingStackFeildsList: any[] = [];
  filteredDeviceList = [];
  UserList: any[] = [];
  loading = false;
  deviceAlreadyExists: boolean;
  selectedUser: string;
  @ViewChild("dt") table: Table;
  selected_field_list: any[] = [];
  isAdmin = true;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private deviceService: DeviceService,
    private sharedService: SharedService,
    private userService: UserService,
    private deviceAllocationService: DeviceAllocationService
  ) {
    this.DeviceForm = fb.group({
      user_id: ["", [Validators.required]],
      device_id: ["", [Validators.required]],
      location_latitude: [""],
      location_longitude: [""],
      fields: [""],
    });
  }

  ngOnInit(): void {
    this.loading = true;
    let role = this.sharedService.LoggedUserConfiguration.userRole;
    this.DeviceForm.valueChanges.subscribe((res) => {});

    this.deviceService.getThingsStackFeilds().subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.ThingStackFeildsList = res.data;
      }
    });
    if (role == "" || role == "admin") {
      this.userService.getAllUsers().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.UserList = res.data;
          this.loading = false;
        }
      });
      this.deviceService.getAllDevices().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.DeviceList = res.data;
          this.filteredDeviceList = res.data;
          this.loading = false;
        }
      });
      this.deviceService.getThingsStackDevices().subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.ThingStackDevicesList = res.data;
        }
      });
    } else {
      this.deviceService
        .getDeviceByUserId(this.sharedService.LoggedUserConfiguration.userId)
        .subscribe((res) => {
          if (res.status == 200 && res.data) {
            this.DeviceList = res.data;
            this.filteredDeviceList = res.data;
            this.loading = false;
          }
        });
      this.deviceAllocationService
        .getDeviceAllocationByUserId(
          this.sharedService.LoggedUserConfiguration.userId
        )
        .subscribe((res) => {
          if (res.status == 200 && res.data) {
            // console.log("Allocated device data", res.data)
            this.ThingStackDevicesList = res.data;
          }
        });
      this.DeviceForm.patchValue({
        user_id: this.sharedService.LoggedUserConfiguration.userId,
      });
      this.isAdmin = false;
    }
    this.DeviceForm.controls.device_id.valueChanges.subscribe((code) => {
      this.deviceAlreadyExists = false;
      if (code != "") {
        this.deviceService
          .getDeviceAlreadyRegistered({ deviceid: code })
          .subscribe((res) => {
            if (res.status == 200 && res.data) {
              this.deviceAlreadyExists = false;
            } else {
              this.deviceAlreadyExists = true;
            }
          });
      }
    });
  }
  get f() {
    return this.DeviceForm.controls;
  }
  saveDevice() {
    this.submitted = true;
    if (this.deviceAlreadyExists) {
      this.notifyError("Error", "Device Id Already Registered !");
      return;
    }

    if (this.DeviceForm.invalid) {
      this.notifyError("Error", "Fill in all the required fields :)");
      return;
    }
    let payload = this.DeviceForm.value;
    this.deviceService.createDevice(payload).subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.DeviceList.push(res.data);
        this.notifySuccess("Success", " Device Added Successfully");
        this.resetForm();
      }
    }),
      (err) => {
        console.log("ERROR", err);
        this.notifyError("Error", "Error adding Device");
      };
  }
  resetForm() {
    if (this.sharedService.LoggedUserConfiguration.userRole == "admin") {
      this.DeviceForm.patchValue({
        device_id: "",
        user_id: "",
        location_latitude: "",
        location_longitude: "",
        fields: "",
      });
    } else {
      this.DeviceForm.patchValue({
        device_id: "",
        location_latitude: "",
        location_longitude: "",
        fields: [],
      });
    }

    this.selectedUser = null;
    this.submitted = false;
    this.selected_field_list = [];
  }
  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail,
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail,
    });
  }
  userSelected(event) {
    if (event != null) {
      this.DeviceForm.patchValue({
        user_id: event._id,
      });
    }
  }
  resetFilters() {
    this.table.reset();
  }
  viewDevice(device) {
    this.router.navigateByUrl(`view-device/${device._id}`);
  }
  deleteDevice(index) {
    this.confirmationService.confirm({
      message: "Are you sure that you want delete device ?",
      accept: () => {
        if (this.table.filteredValue == null) {
          this.deviceService
            .deleteDeviceById(this.DeviceList[index]._id)
            .subscribe(
              (res) => {
                if (res.status == 200 && res.data) {
                  this.DeviceList.splice(index, 1);
                  this.notifySuccess(
                    "Success",
                    "Device deleted successfully :)"
                  );
                }
              },
              (err) => {
                this.notifyError("Error deleting device");
              }
            );
        } else {
          this.deviceService
            .deleteDeviceById(this.table.filteredValue[index]._id)
            .subscribe(
              (res) => {
                if (res.status == 200 && res.data) {
                  index = this.DeviceList.findIndex(
                    (item) => item._id == this.table.filteredValue[index]._id
                  );
                  this.DeviceList.splice(index, 1);
                  this.table.reset();
                  this.notifySuccess(
                    "Success",
                    "Device deleted successfully :)"
                  );
                }
              },
              (err) => {
                this.notifyError("Error deleting device");
              }
            );
        }
      },
    });
  }
  editDevice(deviceid) {
    this.router.navigateByUrl(`edit-device/${deviceid}`);
  }
  removeExtraString(device) {
    if (device.includes("v3/wahab-test-application-01/devices/")) {
      return device.replace("v3/wahab-test-application-01/devices/", "");
    } else {
      return device;
    }
  }
  removeExtraStringParameter(parameter) {
    if (parameter.includes("uplink_message_decoded_payload_")) {
      return parameter.replace("uplink_message_decoded_payload_", "");
    } else {
      return parameter;
    }
  }
}
