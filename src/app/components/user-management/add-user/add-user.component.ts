import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "app/Services/user.service";
import { ConfirmationService, MessageService } from "primeng/api";
import * as _ from "lodash";
import { Table } from "primeng/table";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./tabledemo.scss"],
})
export class AddUserComponent implements OnInit {
  UserForm: FormGroup;
  submitted = false;
  emailAlreadyExists: boolean;
  dobError;
  UserList: any[] = [];
  loading = false;
  @ViewChild("dt") table: Table;
  roles = [
    { label: "Admin", value: "admin" },
    { label: "Normal User", value: "user" },
  ];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private userService: UserService
  ) {
    this.UserForm = fb.group({
      // regNo: ["",],
      name: [
        "",
        [
          Validators.required,
          Validators.pattern(new RegExp("^[a-zA-Z ]*$")),
          Validators.minLength(4),
        ],
      ],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(4)]],
      profile_picture: [""],
      phoneNumber: [
        "",
        [
          Validators.required,
          Validators.pattern(new RegExp("^[0-9]*$")),
          Validators.minLength(11),
          Validators.maxLength(11),
        ],
      ],
      address: [""],
      gender: ["male", Validators.required],
      description: [""],
      role: ["", [Validators.required]],
      canRead: [false],
      canDelete: [false],
      canUpdate: [false],
      canCreate: [false],
    });
  }

  ngOnInit(): void {
    this.UserForm.controls.email.valueChanges.subscribe((email) => {
      this.emailAlreadyExists = false;
      if (email != "") {
        this.userService.getEmailAlreadyRegistered(email).subscribe((res) => {
          if (res.status == 200 && res.data) {
            this.emailAlreadyExists = false;
          } else {
            this.emailAlreadyExists = true;
          }
        });
      }
    });
    this.loading = true;
    this.userService.getAllUsers().subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.UserList = res.data;
        this.loading = false;
      }
    });
  }

  saveUser() {
    this.submitted = true;
    if (this.emailAlreadyExists) {
      this.notifyError("Error", "Email Already Registered !");
      return;
    }
    if (this.UserForm.invalid) {
      this.notifyError("Error", "Fill in all the required feilds :)");
      return;
    }
    if (this.UserForm.value.password.length < 3) {
      this.notifyError("Error", "Password must be aleat 4 characters !");
      return;
    }
    let payload = this.UserForm.value;
    this.userService.createUser(payload).subscribe((res) => {
      if (res.status == 200 && res.data) {
        this.UserList.push(res.data);
        this.notifySuccess("Success", " User Added Successfully");
        this.resetForm();
      }
    }),
      (err) => {
        // console.log("ERROR", err);
        this.notifyError("Error", "Error adding User");
      };
  }
  resetForm() {
    this.UserForm.patchValue({
      regNo: "",
      name: "",
      email: "",
      password: "",
      profile_picture: "",
      phoneNumber: "",
      address: "",
      // dob: "",
      // gender: "",
      description: "",
      role: "",
      canRead: "",
      canDelete: "",
      canUpdate: "",
      canCreate: "",
    });
    this.submitted = false;
  }
  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail,
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail,
    });
  }
  get f() {
    return this.UserForm.controls;
  }

  deleteUser(index) {
    this.confirmationService.confirm({
      message: "Are you sure that you want delete user ?",
      accept: () => {
        if (this.table.filteredValue == null) {
          this.userService.deleteUser(this.UserList[index]._id).subscribe(
            (res) => {
              if (res.status == 200 && res.data) {
                this.UserList.splice(index, 1);
                this.notifySuccess("Success", "User deleted successfully :)");
              }
            },
            (err) => {
              this.notifyError("Error deleting user");
            }
          );
        } else {
          this.userService
            .deleteUser(this.table.filteredValue[index]._id)
            .subscribe(
              (res) => {
                if (res.status == 200 && res.data) {
                  index = this.UserList.findIndex(
                    (item) => item._id == this.table.filteredValue[index]._id
                  );
                  this.UserList.splice(index, 1);
                  this.table.reset();
                  this.notifySuccess("Success", "User deleted successfully :)");
                }
              },
              (err) => {
                this.notifyError("Error deleting user");
              }
            );
        }
      },
    });
  }
  editUser(userid) {
    this.router.navigateByUrl(`edit-user/${userid}`);
  }
}
