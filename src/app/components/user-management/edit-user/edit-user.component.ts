import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "app/Services/user.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-edit-user",
  templateUrl: "./edit-user.component.html",
  styleUrls: ["./tabledemo.scss"],
})
export class EditUserComponent implements OnInit {
  submitted = false;
  activeUser;
  UserForm: FormGroup;
  emailAlreadyExists: boolean;
  changePasswordForm: FormGroup;
  passwordForms: any = [];
  confirmPassError = false;
  passwordsMatched = true;
  currentWrongPassError = false;
  roles = [
    { label: "Admin", value: "admin" },
    { label: "Normal User", value: "user" },
  ];
  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private messageService: MessageService
  ) {
    this.UserForm = fb.group({
      name: [
        "",
        [
          Validators.required,
          Validators.pattern(new RegExp("^[a-zA-Z ]*$")),
          Validators.minLength(4),
        ],
      ],
      email: ["", [Validators.required, Validators.email]],
      phoneNumber: [
        "",
        [
          Validators.required,
          Validators.pattern(new RegExp("^[0-9]*$")),
          Validators.minLength(11),
          Validators.maxLength(11),
        ],
      ],
      address: [""],
      role: ["", [Validators.required]],
    });
    this.changePasswordForm = fb.group({
      current: ["", Validators.required],
      new_password: ["", Validators.required],
      new_password2: ["", Validators.required],
    });
  }

  ngOnInit() {
    const user_id = this.activatedRoute.snapshot.params["user_id"];
    this.getUser(user_id);
    this.UserForm.controls.email.valueChanges.subscribe((email) => {
      this.emailAlreadyExists = false;
      if (email != "" && email != this.activeUser.email) {
        this.userService.getEmailAlreadyRegistered(email).subscribe((res) => {
          if (res.status == 200 && res.data) {
            this.emailAlreadyExists = false;
          } else {
            this.emailAlreadyExists = true;
          }
        });
      }
    });
    this.changePasswordForm.controls.new_password2.valueChanges.subscribe(
      (data) => {
        let pass = this.changePasswordForm.controls.new_password.value;
        if (pass.length < data.length) {
          this.confirmPassError = true;
          this.passwordsMatched = false;
        } else {
          if (data.length > 2) {
            if (pass !== data) {
              this.confirmPassError = true;
              this.passwordsMatched = false;
            } else {
              this.confirmPassError = false;
              this.passwordsMatched = true;
            }
          }
          if (data.length == 0) {
            this.confirmPassError = false;
          }
        }
      }
    );
    this.passwordForms = document.getElementsByName("passForm");
  }
  getUser(user_id) {
    return new Promise<void>((resolve) => {
      this.userService.getUserById(user_id).subscribe(
        (res) => {
          this.activeUser = res.data;
          this.setFormValues();
          resolve();
        },
        (err) => {
          resolve();
        }
      );
    });
  }
  setFormValues() {
    this.UserForm.patchValue({
      name: this.activeUser.name,
      email: this.activeUser.email,
      phoneNumber: this.activeUser.phoneNumber,
      address: this.activeUser.address,
      role: this.activeUser.role,
    });
  }
  notifyError(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: detail,
    });
  }
  notifySuccess(summary: string, detail: string = "") {
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: detail,
    });
  }
  cancel() {
    this.router.navigateByUrl(`add-user`);
  }
  updateUser() {
    if (this.emailAlreadyExists) {
      this.notifyError("Error", "Email Already Registered !");
      return;
    }
    if (this.UserForm.invalid) {
      this.notifyError("Error", "Fill in all the required feilds :)");
      return;
    }

    let payload = this.UserForm.getRawValue();
    this.userService
      .updateUser(this.activeUser._id, payload)
      .subscribe((res) => {
        if (res.status == 200 && res.data) {
          this.notifySuccess("Success", "User Updated Successfully :)");
        }
      }),
      (err) => {
        // console.log("ERROR", err);
        this.notifyError("Error", "Error Updated User");
      };
  }
  savePassword() {
    let submit = this.submitPasswordForm();
    let passwordPayload = this.changePasswordForm.getRawValue();
    delete passwordPayload.new_password2;
    // console.log(passwordPayload)
    if (this.changePasswordForm.invalid) {
      return;
    }

    this.userService
      .changePassword(this.activeUser._id, passwordPayload)
      .subscribe(
        (res) => {
          if (res.status == 200 && res.data) {
            this.currentWrongPassError = false;
            this.messageService.add({
              severity: "success",
              summary: "Password Changed ",
              detail: "Succesfully",
            });
          }
        },
        (err) => {
          this.currentWrongPassError = true;
        }
      );
  }
  submitPasswordForm() {
    var validation = Array.prototype.filter.call(
      this.passwordForms,
      function (form) {
        form.addEventListener(
          "submit",
          function (event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add("was-validated");
          },
          false
        );
      }
    );
  }
  get f() {
    return this.UserForm.controls;
  }
}
