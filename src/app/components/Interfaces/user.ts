export interface ILoggedUserConfiguration {
  userId: string,
  userName: string,
  userRole: string,
  userProfilePicture: string,
  canRead: Boolean,
  canDelete: Boolean,
  canUpdate: Boolean,
  canCreate: Boolean
}