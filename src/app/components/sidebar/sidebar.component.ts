import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'app/Services/shared.service';
import { LocalStorageService } from 'app/Services/Storage/local-storage.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES_ADMIN: RouteInfo[] = [
  { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
  {
    path: '/add-device',
    title: 'Device Management',
    icon: 'library_books',
    class: '',
  },
  { path: '/chart', title: 'Dashboard', icon: 'bubble_chart', class: '' },
  { path: '/alerts', title: 'Alerts', icon: 'notifications', class: '' },
  { path: '/add-user', title: 'User Management', icon: 'person', class: '' },
  {
    path: '/device-allocation',
    title: 'Device Allocation',
    icon: 'airplay',
    class: '',
  },
];
export const ROUTES_USER: RouteInfo[] = [
  { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
  {
    path: '/add-device',
    title: 'Device Management',
    icon: 'library_books',
    class: '',
  },
  { path: '/chart', title: 'Dashboard', icon: 'bubble_chart', class: '' },
  { path: '/alerts', title: 'Alerts', icon: 'notifications', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(
    private localStorageService: LocalStorageService,
    private sharedService: SharedService,
    private router: Router
  ) {}

  ngOnInit() {
    this.checkUserRole();
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
  checkUserRole() {
    let role = this.sharedService.LoggedUserConfiguration.userRole;

    if (role == 'admin') {
      this.menuItems = ROUTES_ADMIN.filter((menuItem) => menuItem);
    } else if (role == 'user') {
      this.menuItems = ROUTES_USER.filter((menuItem) => menuItem);
    } else {
      this.router.navigateByUrl('/login');
    }
  }
}
