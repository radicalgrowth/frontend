import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MongoServerBasicService } from './mongo-server-basic.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends MongoServerBasicService {

  constructor(private http: HttpClient) {
    super(http);
    this.methodName = "users"
  }
  public createUser(payload): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}`, payload);
  }
  public getUserById(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public getAllUsers(): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}`);
  }
  public deleteUser(id: string): Observable<any> {
    return this.http.delete<any>(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public updateUser(id, body): Observable<any> {
    return this.http.put(`${this.webapiURL}${this.methodName}/${id}`, body);
  }
  public loginUser(body): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}/login`, body);
  }
  public logOutUser(body): Observable<any> {
    let payload = {
      token: body.token
    }
    return this.http.post(`${this.webapiURL}${this.methodName}/${body.id}/logout`, payload);
  }
  public changePassword(id, body): Observable<any> {
    //body contains current, new_password
    return this.http.put(`${this.webapiURL}${this.methodName}/${id}/change_password`, body);
  }
  public silentLoginUser(body): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}/login/silent`, body);
  }
  public getUserByRegId(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/getbyregid/${id}`);
  }
  public getUserByRole(role): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/getbyrole/${role}`);
  }
  public signUpUser(body): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}/signup`, body);
  }
  public getUserRequests(active = true): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/user_requestactive/${active}`);
  }
  public getAllUserRequests(): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/user_request`);
  }
  public getUserRequestById(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/user_request/${id}`);
  }
  public inactiveUserRequest(id): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}/user_request/${id}/inactive`, {});
  }
  public sendVerificationLink(email): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}/forgot-password/`, {
      email: email
    });
  }
  public changePasswordWithVerification(id, new_password): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}/change-pass-verification/${id}`, {
      new_password: new_password
    });
  }
  public getEmailAlreadyRegistered(email): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/check-email/${email}`);
  }
}
