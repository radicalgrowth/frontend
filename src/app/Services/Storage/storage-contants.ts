export class StorageConstants {

    public static readonly CURRENT_USER = 'current_user';

    public static readonly USER_ID = 'user_id';

    public static readonly TOKEN = 'token';

    public static readonly USER_KEY = 'user';

}