import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BasicService {
  webapiURL: string = 'http://localhost:3000/';
  // webapiURL: string = "http://localhost:5000/";
  // webapiURL: string = "http://192.168.0.2:5000/";
  options: any;
  protected methodName;

  constructor(private $http: HttpClient) {}
  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
