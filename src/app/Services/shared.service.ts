import { Injectable } from '@angular/core';
import { ILoggedUserConfiguration } from 'app/components/Interfaces/user';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
  LoggedUserConfiguration: ILoggedUserConfiguration = {
    userId: "",
    userName: "",
    userRole: "",
    userProfilePicture: "",
    canCreate: false,
    canUpdate: false,
    canDelete: false,
    canRead: false

  };
  _LoggedUserConfiguration = new BehaviorSubject(this.LoggedUserConfiguration);

  constructor() {
    this._LoggedUserConfiguration.next(this.LoggedUserConfiguration);
  }
  public setLoggedInUser(user: ILoggedUserConfiguration) {
    this.LoggedUserConfiguration = user;
    this._LoggedUserConfiguration.next(this.LoggedUserConfiguration);
  }
  public setProfilePicture(picture_path) {
    this.LoggedUserConfiguration.userProfilePicture = picture_path;
    this._LoggedUserConfiguration.next(this.LoggedUserConfiguration);
  }
  public resetConfiguration() {
    this.LoggedUserConfiguration.userId = "";
    this.LoggedUserConfiguration.userName = "";
    this.LoggedUserConfiguration.userRole = "";
    this.LoggedUserConfiguration.userProfilePicture = "";
    this.LoggedUserConfiguration.canCreate = false;
    this.LoggedUserConfiguration.canUpdate = false;
    this.LoggedUserConfiguration.canDelete = false;
    this.LoggedUserConfiguration.canRead = false;
    this._LoggedUserConfiguration.next(this.LoggedUserConfiguration);
  }

}
