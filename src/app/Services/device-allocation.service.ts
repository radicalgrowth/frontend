import { Injectable } from '@angular/core';
import { MongoServerBasicService } from './mongo-server-basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeviceAllocationService extends MongoServerBasicService {

  constructor(private http: HttpClient) {
    super(http);
    this.methodName = "deviceAllocations"
  }
  public createDeviceAllocation(payload): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}`, payload);
  }
  public getDeviceAllocationById(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public getAllDeviceAllocations(): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}`);
  }
  public deleteDeviceAllocationById(id: string): Observable<any> {
    return this.http.delete<any>(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public updateDeviceAllocationById(id, body): Observable<any> {
    return this.http.put(`${this.webapiURL}${this.methodName}/${id}`, body);
  }
  public getDeviceAllocationByUserId(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/getdevicesbyuserid/${id}`);
  }
}
