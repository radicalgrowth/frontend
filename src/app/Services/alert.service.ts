import { Injectable } from '@angular/core';
import { MongoServerBasicService } from './mongo-server-basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService extends MongoServerBasicService {

  constructor(private http: HttpClient) {
    super(http);
    this.methodName = "alerts"
  }
  public createAlert(payload): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}`, payload);
  }
  public getAlertById(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public getAllAlerts(): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}`);
  }
  public deleteAlertById(id: string): Observable<any> {
    return this.http.delete<any>(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public updateAlertById(id, body): Observable<any> {
    return this.http.put(`${this.webapiURL}${this.methodName}/${id}`, body);
  }
}
