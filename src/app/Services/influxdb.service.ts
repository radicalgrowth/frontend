import { Injectable } from '@angular/core';
import { BasicService } from './basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InfluxdbService extends BasicService {

  constructor(private http: HttpClient) {
    super(http);
    this.methodName = ""
   }
   public getAllDatabases(): Observable<any> {
    return this.http.get(`${this.webapiURL}getalldatabases`);
  }
  }
