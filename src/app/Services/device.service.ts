import { Injectable } from '@angular/core';
import { MongoServerBasicService } from './mongo-server-basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DeviceService extends MongoServerBasicService {
  constructor(private http: HttpClient) {
    super(http);
    this.methodName = 'devices';
  }
  public createDevice(payload): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}`, payload);
  }
  public getDeviceById(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public getDeviceByUserId(id): Observable<any> {
    return this.http.get(
      `${this.webapiURL}${this.methodName}/getdevicesbyuserid/${id}`
    );
  }
  public getAllDevices(): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}`);
  }
  public deleteDeviceById(id: string): Observable<any> {
    return this.http.delete<any>(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public updateDeviceById(id, body): Observable<any> {
    return this.http.put(`${this.webapiURL}${this.methodName}/${id}`, body);
  }
  public getDeviceAlreadyRegistered(payload): Observable<any> {
    return this.http.post(
      `${this.webapiURL}${this.methodName}/check-deviceid`,
      payload
    );
  }
  public getDataByDeviceId(id, field, time): Observable<any> {
    let payload = {
      id: id,
      field: field,
      time: time,
    };
    return this.http.post(
      `${this.webapiURL}${this.methodName}/getdatabydeviceid`,
      payload
    );
  }
  public getmultipledevicedata(devices, field, time): Observable<any> {
    let payload = {
      devices: devices,
      field: field,
      time: time,
    };
    return this.http.post(
      `${this.webapiURL}${this.methodName}/getmultipledevicedata`,
      payload
    );
  }
  public getParametersByDeviceId(id): Observable<any> {
    return this.http.get(
      `${this.webapiURL}${this.methodName}/getparametersbydeviceid/${id}`
    );
  }
  getPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (resp) => {
          resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  public getLastDataByDeviceId(id, field): Observable<any> {
    return this.http.get(
      `${this.webapiURL}${this.methodName}/getlastdatabydeviceid/${id}/${field}`
    );
  }
  public getThingsStackDevices(): Observable<any> {
    return this.http.get(
      `${this.webapiURL}${this.methodName}/getthingstackdevices`
    );
  }
  public getThingsStackFeilds(): Observable<any> {
    return this.http.get(
      `${this.webapiURL}${this.methodName}/getthingstackfields`
    );
  }
}
