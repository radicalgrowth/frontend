import { Injectable } from '@angular/core';
import { MongoServerBasicService } from './mongo-server-basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ChartService extends MongoServerBasicService {

  constructor(private http: HttpClient) {
    super(http);
    this.methodName = "charts"
  }
  public createChart(payload): Observable<any> {
    return this.http.post(`${this.webapiURL}${this.methodName}`, payload);
  }
  public getChartById(id): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public getAllCharts(): Observable<any> {
    return this.http.get(`${this.webapiURL}${this.methodName}`);
  }
  public deleteChartById(id: string): Observable<any> {
    return this.http.delete<any>(`${this.webapiURL}${this.methodName}/${id}`);
  }
  public updateChartById(id, body): Observable<any> {
    return this.http.put(`${this.webapiURL}${this.methodName}/${id}`, body);
  }
}
